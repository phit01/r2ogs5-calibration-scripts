#devtools::install_git("https://gitlab.opengeosys.org/ogs5/r2ogs5")
library(r2ogs5)
library(dplyr)
nr <- commandArgs(trailingOnly = TRUE)
sim_dir <- "ogs-input/split3"
gwf3 <- create_ogs5(sim_name = "gwf3",
                    sim_id = 1L,
                    sim_path = paste0(sim_dir, "/opt_run", nr))

gwf3 <- input_add_blocs_from_file(ogs5_obj = gwf3,
                                  sim_basename = "Ashi_3regions",
                                  filename = "all",
                                  file_dir = paste0(sim_dir, "/Ashi3"))

# str(gwf3$input$mmp)
# gwf3$input$msh$FEM_MSH1$ELEMENTS$pris$material_id %>% range
ogs5_write_inputfiles(gwf3)

f <- function(ogs5_obj, exp_data) {
  # target function to optimize
  # generally some sort of mean squared error
  
  # data cleaning; transformation to meet structure of exp_data
  ogs5_obj$output <- plyr::ldply(ogs5_obj$output,
                                 function(tbl) {
                                   return(tbl[[1]])
                                 }) %>%
    dplyr::filter(TIME == 1)
  ogs5_obj$output$well <- c(1:10, 12:15)
  
  se <- sapply(seq_along(exp_data$well), function(i) {
    ((exp_data$head[i] - ogs5_obj$output$HEAD[i])**2)
  })
  return(mean(se))
}
calibration_set <- cal_create_calibration_set(
  c("mmp$MEDIUM_PROPERTIES1$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel1
  c("mmp$MEDIUM_PROPERTIES2$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay1
  c("mmp$MEDIUM_PROPERTIES3$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand1
  c("mmp$MEDIUM_PROPERTIES4$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # soil1
  
  c("mmp$MEDIUM_PROPERTIES5$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel2
  c("mmp$MEDIUM_PROPERTIES6$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay2
  c("mmp$MEDIUM_PROPERTIES7$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand2
  c("mmp$MEDIUM_PROPERTIES8$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # soil2
  
  c("mmp$MEDIUM_PROPERTIES9$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel3
  c("mmp$MEDIUM_PROPERTIES10$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay3
  c("mmp$MEDIUM_PROPERTIES11$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand3
  c("mmp$MEDIUM_PROPERTIES12$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3) # soil3
)

# sample starting parameters from 0-1
init <- cal_sample_parameters(calibration_set,
                              n_samples = 1,
                              interval_01 = TRUE)


data("groundwater_exp")
out_names <- paste0("OUTPUT", c(2:11, 13:16))

# initialize counting file
# cnt_dir <- paste0(sim_dir, "/op_count", nr, ".txt")
# write("0", file = cnt_dir)

op_history <<- NULL
optfun <- function(x) {
  # build parameter df
  df <- cbind(calibration_set, x)
  # transform to real range
  df <- r2ogs5:::from01(df, scale_fun = log10, unscale_fun = function(x) {10**x})
  
  err <- cal_simulation_error(df,
                              exp_data = groundwater_exp,
                              ogs5_obj = gwf3,
                              ogs_exe =  "/usr/local/bin/ogs_fem",
                              outbloc_names = out_names,
                              objective_function = f)
  # cnt <- scan(cnt_dir, quiet = TRUE)
  # write(cnt, cnt_dir)
  op_history <<- c(op_history, err)
  return(err)
}
op <- optim(par = init$sample_1,
            fn = optfun,
            method = "L-BFGS-B",
            lower = 0,
            upper = 1
            #, 
            # control = list(maxit = 1)
)

# op$count <- scan(cnt_dir, quiet = TRUE)
op$history <- op_history

save(op, file = paste0(sim_dir, "/optim_", nr, "_gwf3.RData"))
