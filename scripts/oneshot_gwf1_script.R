#devtools::install_git("https://gitlab.opengeosys.org/ogs5/r2ogs5")
library(r2ogs5)
library(dplyr)
sim_dir <- "ogs-input/split1"
nr <- commandArgs(trailingOnly=TRUE)

gwf1 <- create_ogs5(sim_name = "gwf1",
                    sim_id = 1L,
                    sim_path = paste0(sim_dir, "/oneshot", nr))
gwf1 <- input_add_blocs_from_file(ogs5_obj = gwf1,
                                  sim_basename = "Ashi",
                                  filename = "all",
                                  file_dir = "ogs-input/split1/Ashi1")

ogs5_write_inputfiles(gwf1)

f <- function(ogs5_obj, exp_data) {
    # target function to optimize
    # generally some sort of mean squared error

    # data cleaning; transformation to meet structure of exp_data
    ogs5_obj$output <- plyr::ldply(ogs5_obj$output,
                                   function(tbl) {
                                       return(tbl[[1]])
                                   }) %>%
        dplyr::filter(TIME == 1)
    ogs5_obj$output$well <- c(1:10, 12:15)

    if (!all(ogs5_obj$output$well == ogs5_obj$output$well)) {
        stop("data does not match")
    }
    se <- sapply(seq_along(exp_data$well), function(i) {
        ((exp_data$head[i] - ogs5_obj$output$HEAD[i])**2)
    })
    return(mean(se))
}
calibration_set <- cal_create_calibration_set(
    c("mmp$MEDIUM_PROPERTIES1$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel1
    c("mmp$MEDIUM_PROPERTIES2$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay1
    c("mmp$MEDIUM_PROPERTIES3$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand1
    c("mmp$MEDIUM_PROPERTIES4$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3) # soil1

)    
    

# sample starting parameters from calibration set
init <- cal_sample_parameters(calibration_set,
                              n_samples = 136,
                              interval_01 = FALSE,
                              scale_fun = log10,
                              unscale_fun = function(x){10**x}
)


data("groundwater_exp")
out_names <- paste0("OUTPUT", c(2:11, 13:16))
bo <- cal_bayesOpt(par_init = init,
                   kappa = 0,
                   max_it = 1, # surrogate model is only fitted once
                   exp_data = groundwater_exp,
                   ogs5_obj = gwf1,
                   outbloc_names = out_names,
                   ogs_exe = "/usr/local/bin/ogs",
                   objective_function = f,
                   ensemble_path = paste0(sim_dir, "/ensembles"),
                   ensemble_cores = 17,
		           ensemble_name = paste0("gwf1_136_", nr),
                   scale_fun = log10,
                   unscale_fun = function(x) 10**x)

save(bo, file = paste0(sim_dir, "/oneshot136", "_gwf1_", nr, ".RData"))
