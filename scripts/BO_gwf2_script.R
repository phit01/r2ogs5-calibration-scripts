#devtools::install_git("https://gitlab.opengeosys.org/ogs5/r2ogs5")
library(r2ogs5)
library(dplyr)
sim_dir <- "ogs-input/split2"

nr <- commandArgs(trailingOnly=TRUE)

gwf2 <- create_ogs5(sim_name = "gwf2",
                    sim_id = 1L,
                    sim_path = paste0(sim_dir, "/bo_run", nr))
gwf2 <- input_add_blocs_from_file(ogs5_obj = gwf2,
                                  sim_basename = "Ashi_2regions",
                                  filename = "all",
                                  file_dir = paste0(sim_dir, "/Ashi2"))

ogs5_write_inputfiles(gwf2)

f <- function(ogs5_obj, exp_data) {
    # target function to optimize
    # generally some sort of mean squared error

    # data cleaning; transformation to meet structure of exp_data
    ogs5_obj$output <- plyr::ldply(ogs5_obj$output,
                                   function(tbl) {
                                       return(tbl[[1]])
                                   }) %>%
        dplyr::filter(TIME == 1)
    ogs5_obj$output$well <- c(1:10, 12:15)

    if (!all(ogs5_obj$output$well == ogs5_obj$output$well)) {
        stop("data does not match")
    }
    se <- sapply(seq_along(exp_data$well), function(i) {
        ((exp_data$head[i] - ogs5_obj$output$HEAD[i])**2)
    })
    return(mean(se))
}
calibration_set <- cal_create_calibration_set(
    c("mmp$MEDIUM_PROPERTIES1$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel1
    c("mmp$MEDIUM_PROPERTIES2$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay1
    c("mmp$MEDIUM_PROPERTIES3$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand1
    c("mmp$MEDIUM_PROPERTIES4$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # soil1

    c("mmp$MEDIUM_PROPERTIES5$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-4, 1.0e-2), # gravel1
    c("mmp$MEDIUM_PROPERTIES6$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-9, 1.0e-4), # clay1
    c("mmp$MEDIUM_PROPERTIES7$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3), # sand1
    c("mmp$MEDIUM_PROPERTIES8$PERMEABILITY_TENSOR", "ISOTROPIC", 1.0e-7, 1.0e-3) # soil1
)    
    

# sample starting parameters from calibration set
init <- cal_sample_parameters(calibration_set,
                              n_samples = 24,
                              interval_01 = FALSE,
                              scale_fun = log10,
                              unscale_fun = function(x){10**x}
)


data("groundwater_exp")
out_names <- paste0("OUTPUT", c(2:11, 13:16))
bo <- cal_bayesOpt(par_init = init,
                   kappa = "log_t",
                   max_it = 200,
                   exp_data = groundwater_exp,
                   ogs5_obj = gwf2,
                   outbloc_names = out_names,
                   ogs_exe = "/usr/local/bin/ogs",
                   objective_function = f,
                   ensemble_path = paste0(sim_dir, "/ensembles"),
                   ensemble_cores = 12,
		           ensemble_name = paste0("gwf2_", nr),
                   scale_fun = log10,
                   unscale_fun = function(x) 10**x)

save(bo, file = paste0(sim_dir, "/split2_12-100_bo_nr", nr, ".RData"))

jpeg(filename = paste0(sim_dir, "/split2_12-100_boPlot_nr", nr, ".jpg"), width = 16, height = 10, units = "cm", res = 200)
plot(bo)
dev.off() 
