# r2ogs5 - calibration scripts

The scripts that produced the results from the paper "r2ogs5: Calibration of Numerical Groundwater Flow Models with Bayesian Optimization in R" are found here. Those interested on how the results presented in the paper were obtained may have a look into the scripts. 

Besides the scripts, the OpenGeoSys5 input files that constitute the the groundwater for 1, 2 and 3 regions respectively are found here.
The [singularity](https://sylabs.io/guides/3.7/user-guide/index.html) container definition file used to deploy the scripts on the cluster is also available. From the file, a container image can be built via:  

 `singularity build <container_name.img> r2ogs5_container.def`  

 The container contains the API r2ogs5 already installed, and a compiled ogs executable in the directory `/usr/local/bin`.

## Deployment to Cluster

The scripts can be executed in the singularity container for example by running:  

`singularity exec /path/to/r2ogs5_container.img Rscript /path/to/BO_gwf2_script.R <run_number>`
